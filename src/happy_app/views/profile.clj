(ns happy-app.views.profile
  (:use [net.cgrand.enlive-html]))

(deftemplate profile-page "profile.html"
  [user]
  [:title]  (content (:name user))
  [:h1]     (content (:name user)))

(defn blah
  []
  (str "test"))
