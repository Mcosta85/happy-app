(ns happy-app.web
  (:require [compojure.core :refer [defroutes GET]]
            [org.httpkit.server :refer [run-server]]
            [happy-app.controllers.user :as user]
            [happy-app.controllers.auth :as auth]
            [compojure.route :as route]
            [ring.middleware.json :as json]
            [compojure.handler :as handler])
  (:gen-class))

(defroutes app-routes 
           user/routes
           auth/routes
          (GET "/" [] (str "Hello world")))

(def app 
  (->
    (handler/api app-routes)
    (json/wrap-json-params)
    (json/wrap-json-response)))


(defn -main []
  (let [port (Integer. (or (System/getenv "PORT") "8080"))]
    (run-server app {:port port})))
