(ns happy-app.models.user
  (:require [clojurewerkz.scrypt.core   :as scrypt]
            [clojure.tools.logging      :as log]
            [happy-app.utilities.db     :as db]
            [happy-app.utilities.common :as utils]
            [happy-app.data.access      :as data]))

(def ^:private security 
  "SELECT password, salt
   FROM user_base
   WHERE id = ?")

(def ^:private user-by-id
  "SELECT u.email, u.name, u.is_verified as verified, s.name as status 
  FROM user_base as u
  JOIN status as s
  on u.status_id = s.id
  WHERE u.id = ?")

(def ^:private id-by-email
  "SELECT id
  FROM user_base
  WHERE email = ?")

(defn select
  "Summary:     Gets a single user from the db.
   Assertions:  id is a positive number."

  [id query]
  (future (log/info "selecting user: " id "'s security info."))
  (data/fetch id
              query
              (db/connection)))

(defn select-user-security-info
  "Summary: Gets security data for the given user." 
  [id]
  (select id security))

(defn select-id-by-email
  "Summary: Gets a user's id by their email address."
  [email]
  (select email id-by-email))

(defn create!
  "Summary: Creates a user's account in the db."

  [{:keys [password email] :as request}]
  (future (log/info "Creating user: {request: " request))
  (let [salt (utils/generate-salt)
        hashed-password (utils/hash-salt-password salt password)
        user (assoc request :salt salt
                            :password hashed-password
                            :status_id utils/pending
                            :role_id utils/standard)]
    (data/create! user :user_base (db/connection))))

(defn update!
  "Summary:     Modifies the users record in the db.
   Assertions:  id is a positive number."

  [id updated]
  {:pre [(pos? id)]}
  (future (log/info "Modifying user: {request: " updated))
  (data/update! id
                (utils/sanitize-keys updated)
                :user_base
                (db/connection)))

(defn validate-password
  "Summary:     Validates that the user has entered their correct password."

  [user password]
  (future (log/info "Validating user: {name:" user ", password: " password"}"))
  (let [id (:id (into {}
                 (select-id-by-email user)))
        user (into {} (select-user-security-info id))
        salted-hash (str password (:salt user))]
    (scrypt/verify salted-hash (:password user))))
