(ns happy-app.models.auth
  (:require [happy-app.models.user  :as user]
            [buddy.sign.jws         :as jws]
            [slingshot.slingshot    :refer [try+]]
            [clj-time.core          :refer  [now plus days]]))

(def ^:private token-secret "kibUIH98u08UIV(*0bi)")

(defn create-claims
  "Summary: creates a user token to be signed via JWS. Default token expiry is 1 day."
  [id]
  {:id id
   :iss "SingleStep.HappyApp"
   :exp (plus (now) (days 1))
   :iat (now)})

(defn sign-claims
  "Summary: signs a set of claims using the user's salt."
  [claims]
  (jws/sign claims token-secret)) 

(defn extract-token
  "Summary: unsigns the token allowing for retrieval of its contents."
  [token]
  (try+
    (jws/unsign token token-secret)
    (catch [:type :validation] e
      (str "Error: " (:message e)))))

(defn is-authorized
  "Summary: verifies a user is authorized to perform an action on a given resource."
  [id token]
  (let [auth-id (:id (extract-token token))]
    (= auth-id id)))
