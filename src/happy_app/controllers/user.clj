(ns happy-app.controllers.user
  (:use ring.util.response)
  (:require [compojure.core             :refer [context defroutes GET POST PUT DELETE]]
            [compojure.route            :as route]
            [compojure.coercions        :refer [as-int]]
            [ring.middleware.json       :refer [wrap-json-body]]
            [ring.util.response         :refer [response]]
            [happy-app.utilities.common :as util]
            [happy-app.models.auth      :as auth]
            [happy-app.models.user      :as model]))

(defn ^:private fetch
  "Summary:   Pulls a user record from the database."

  [id]
  (let [response (model/select id)]
    (util/process-response response :get)))

(defn ^:private create
  "Summary:   Adds the user to the database."
  [request]
  (let [params  (:json-params request)
        cleaned (util/clean-request params)]
    (util/process-response (model/create! cleaned) :post)))

(defn ^:private modify
  "Summary: Updates a user record in the database."
  [id request]
  (if (auth/is-authorized id (get-in request [:headers :token]))
    (let [cleaned (util/clean-sensitive-request (:json-params request))
          result  (when (not (empty? cleaned))
                        (model/update! id
                                       cleaned))
          response (if (= false (:success result))
                        result
                        (util/parse-row-count result))]

      (util/process-response response
                             :put))
    {:success false :message "Unauthorized to perform this action."}))

(def token (auth/sign-claims (auth/create-claims 25)))
(def mock-request {:headers {:token token}})
(modify 25 {:headers {:token token} :json-params {:status-id util/active}}) 

(defn ^:private delete
  "Summary: Sets the users status to deleted"
  [id request]
    (modify id (util/add-to-request request :status-id util/deleted)))

(defn ^:private verify
  "Summary: Verifies the users account. This means they got the registration email and did in fact wish to engage."
  [id request]
  (modify id (util/add-to-request request :is-verified true)))

(defn ^:private activate
  "Summary: Activates the users account."
  [id request]
    (modify id (util/add-to-request request :status-id util/active)))

(defn ^:private update-password
  "Summary: Modifies the user's password. This has it's own function because modifying the password needs to be carefully controlled."
  [id request]
  (if (auth/is-authorized id (get-in request [:headers :token]))
    (let [salt (util/generate-salt)
          password (get-in request [:json-params :password])
          hashed-password (util/hash-salt-password salt password)
          result (model/update! id 
                                {:password hashed-password
                                 :salt salt})
          response (if (= false (:success result))
                        result
                        (util/parse-row-count result))]

    (util/process-response  response
                            :put))
    {:success false :message "Unauthorized to perform this action."}))
    
(defroutes routes
  (route/resources "/")

  (context "/user" []
    (POST "/" request (create request))
      (context "/:id" [id]
        (GET "/" [] (fetch (as-int id)))
        (PUT "/" request (modify (as-int id) 
                                 request))
        (DELETE "/" [] (delete (as-int id)))
        (PUT "/verify" [] (verify (as-int id)))
        (PUT "/activate" [] (activate (as-int id)))
        (PUT "/change-password" request (update-password (as-int id)
                                                         request)))))
