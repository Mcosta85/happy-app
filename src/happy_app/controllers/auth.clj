(ns happy-app.controllers.auth
  (:require [compojure.core             :refer [context defroutes POST]]
            [compojure.route            :as route]
            [happy-app.utilities.common :as util]
            [happy-app.models.user      :as user]
            [happy-app.models.auth      :as auth]))

(defn login
  "Summary: authenticates a user to the system and generates a tokenfor their session."
  [{:keys [user password] :as request}]
  (if (user/validate-password user password)
    (let [id (:id (into {} (user/select-id-by-email user)))
          user (into {} (user/select-user-security-info id))
          salt (:salt user)]
    (auth/sign-claims (auth/create-claims id)))))

(defroutes routes
  (POST "/login" request (login (util/clean-request 
                                  (:json-params request)))))
