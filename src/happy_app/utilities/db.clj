(ns happy-app.utilities.db
    (:import com.mchange.v2.c3p0.ComboPooledDataSource)
    (require [clojure.java.jdbc     :as jdbc]
             [clojure.tools.logging :as log]))

(def spec {:classname "org.postgresql.Driver"
           :subprotocol "postgresql"
           :subname "//localhost:5432/positive"
           :user "dbuser"
           :password "zI4PhzZmROZELP0s9AfKkg-_6M"})

(defn pool
  [spec]
  (let [cpds (doto (ComboPooledDataSource.)
                   (.setDriverClass (:classname spec))
                   (.setJdbcUrl (str "jdbc:" (:subprotocol spec) ":" (:subname spec)))
                   (.setUser (:user spec))
                   (.setPassword (:password spec))
                   ;; expire excess connections after 30 minutes of inactivity:
                   (.setMaxIdleTimeExcessConnections (* 30 60))
                   ;; expire connections after 3 hours of inactivity
                   (.setMaxIdleTime (* 3 60 60)))]
    {:datasource cpds}))
     
(def pooled-db (delay (pool spec)))

(defn connection [] 
  (future (log/info "getting a connection from pool."))
  @pooled-db)
