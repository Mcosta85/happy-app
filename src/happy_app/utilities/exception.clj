(ns happy-app.utilities.exception
  (:require [happy-app.utilities.common :as utils]
            [clojure.tools.logging      :as log]
            [clojure.string             :as string]))

;; PSQL Exception Handling

(defn extract-message
 "Extracts the error message from a PSQL Exception."
  [exception]
  (->> exception
    (.getServerErrorMessage)
    (.getMessage)))

(defn constraint-violated?
  "Searches for a regex in an extracted PSQL Exception."
  [constraint exception]
  (->> (extract-message exception)
       (re-matcher constraint)
        re-find
        utils/not-nil?))

;; PSQL Exception Messages
(def foreign-key #"violates foreign key constraint")
(def duplicate-key #"duplicate key value violates unique constraint")
(def invalid-operator #"operator does not exist")
(def invalid-type #"but expression is of type")
(def invalid-relation #"does not exist")
(def foreign-key-delete #"is still referenced from table")
(def null-value #"null value in column")
(def permission-denied #"Permission denied")
(def value-too-long #"value too long for type")

;; PSQL Exception Partial Functions
(def foreign-key-constraint-violated?
  (partial constraint-violated? foreign-key))

(def duplicate-key-constraint-violated?
  (partial constraint-violated? duplicate-key))

(def invalid-operator-constraint-violated?
  (partial constraint-violated? invalid-operator))

(def invalid-type-constraint-violated?
  (partial constraint-violated? invalid-type))

(def invalid-relation-constraint-violated?
  (partial constraint-violated? invalid-relation))

(def foreign-key-delete-constraint-violated?
  (partial constraint-violated? foreign-key-delete))

(def null-value-constraint-violated?
  (partial constraint-violated? null-value))

(def permission-denied-constraint-violated?
  (partial constraint-violated? permission-denied))

(def value-too-long-constraint-violated?
  (partial constraint-violated? value-too-long))

(defn parse-exception
    [exception]
      (println (count (string/split (.getMessage exception) #"(\.)"))))

(defn handle-exception
  "Handles all exceptions from a central location. Any logging or formatted error returning takes place right here."
  [e]
  (future (log/info (.getMessage e)))
  (cond
    (= java.lang.NullPointerException (type e))
      {:success false :message "Null values aren't allowed."}

    (= java.lang.IllegalArgumentException (type e))
      {:success false :message e}

    (= java.sql.BatchUpdateException (type e))
      (handle-exception (.getNextException e))

    (invalid-relation-constraint-violated? e)
      {:success false :message "Column or relation does not exist."}

    (invalid-type-constraint-violated? e)
      {:success false :message "Invalid input type."}

    (duplicate-key-constraint-violated? e)
      {:success false :message "This object already exists."}

    (invalid-operator-constraint-violated? e)
      {:success false :message "Invalid operator."}

    (null-value-constraint-violated? e)
      {:success false :message "Null values aren't allowed."}

    (permission-denied-constraint-violated? e)
      {:success false :message "You do not have access to perform the selected action."}

    (foreign-key-constraint-violated? e)
      {:success false :message "Referenced object does not exist."}

    (value-too-long-constraint-violated? e)
      {:success false :message "Value is too long for the field."}
    
    :else
      {:success false :message "Unknown error"}))
