(ns happy-app.utilities.common
  (:require [clojure.string :as string]
            [clojure.walk :as walk]
            [clojurewerkz.scrypt.core :as scrypt]
            [crypto.random :as crypto]))

(def active 1)
(def inactive 2)
(def deleted 3)
(def pending 4)
(def needs-review 5)

(def admin 1)
(def standard 2)

(def cpu-cost 16384)
(def ram-cost 8)
(def parallelism 1)

(def default-page-size 20)
(def default-offset 0)

(def sensitive-fields [:password :salt])

(def not-nil? (comp not nil?))

(def status-codes {:get     {:success 200
                             :failure 404}
                   :post    {:success 201
                             :failure 400}
                   :put     {:success 204
                             :failure 404}
                   :delete  {:success 204
                             :failure 404}})

(defn rows-affected?
  "Returns a boolean whose value depends on whether any rows were affected. Pre-conditions are present on the result being non-nil and a number. Throws an assertion error if violated."
  [result]
  {:pre [(not-nil? result)
         (number? result)]}
  (if
    (pos? result)
    true
    false))

(defn parse-row-count
  "Turns the seq of rows affected into a map reflecting the same information. Pre-conditions are present on the seq containing all numbers. Throws an assertion error if violated."
  [update-result]

  (let [result (reduce + update-result)]
    {:success (:success update-result true)
     :rows-affected result}))

(defn sanitize-input
  "Summary: Converts a string number to a Number type."
  [validate-fn convert-fn input]
  (if (validate-fn input) input
                          (convert-fn input)))

(defn sanitize-keys
  "Removes any '-' characters and replaces them with '_'. Pre-condition exists on the in-map being a Clojure map. Throws an assertion error if violated."
  [in-map]
  {:pre [(map? in-map)]}

  (let [clean-keys (map (comp #(string/replace % "-" "_") name) (keys in-map))]
    (apply hash-map
      (interleave
        (map keyword clean-keys)
        (vals in-map)))))

(defn strip-sensitive-data
  "Removes any sensitive fields from maps. Pre-condition exists on the input being a Clojure map. Throws an assertion error if violated."
  ([in-map]
    (strip-sensitive-data in-map sensitive-fields))

  ([in-map sensitive-keys]
   {:pre [(map? in-map)]}
    (apply dissoc in-map sensitive-keys)))

(defn apply-rules
  "takes a sequence of rules functions and runs them against an input map"
  [rules s]
  (loop [rule-fns rules request s]
    (if (empty? rule-fns) request
      (recur (rest rule-fns) ((first rule-fns) request)))))

(defn generate-salt
  "Summary: Generates a cryptographic random 32 bit salt."
  []
  (crypto/base64 32))

(defn hash-salt-password
  "interleaves the salt value with the password and scrypts."
  [salt password]
  (let [salted-password (str password salt)]
    (scrypt/encrypt (str salted-password)
                    cpu-cost
                    ram-cost
                    parallelism)))

(defn successful?
  "Summary: Returns a value indicating the success of a database request."

  [response]
  (:success response true))

(defn response-to-map
  "Summary: Converts the seq resulting from a database action into a map."
  [response]
  (into {} response))

(defn set-success
  "Summary: Sets the success key in the response map based on the outcome of the database action."
  [response]
  (if (not (:success response))
    (assoc response :success true)
    response))

(defn set-status
  "Summary: Sets the HTTP status for the response."

  [result action]
  (cond
    (not (nil? (:status result)))       (:status result)
    (or (= result nil)
        (= (:success result) false))    400
    (or (empty? result)
        (= (:rows-affected result) 0))  (get-in status-codes
                                                [action :failure])
    (not (empty? result))               (get-in status-codes
                                                [action :success])
    :else                               500))

(defn format-response
  "Summary: Adds the appropriate status and body to the response."
       
  [body status]
  {:status status
   :body body})

(defn process-response
  "Summary: runs a raw response through a series of functions to create a finished response map."

  [response action]
  (-> response
      response-to-map
      strip-sensitive-data
      (format-response (set-status response action))))

(defn add-to-request
  "Summary: adds a key value pair to the json-params of a request."
  [request key value]
  (assoc-in request [:json-params key] value))

;; Rules for preparing an inbound request
(def basic-request-rules [walk/keywordize-keys
                          sanitize-keys])

(def sensitive-request-rules [walk/keywordize-keys
                              sanitize-keys
                              strip-sensitive-data])

(def clean-request (partial apply-rules basic-request-rules))

(def clean-sensitive-request (partial apply-rules
                                      sensitive-request-rules))
(def sanitize-input-number (partial sanitize-input  number?
                                                    read-string))
