(ns happy-app.data.access
  (:require [clojure.java.jdbc              :as jdbc]
            [clojurewerkz.scrypt.core       :as scrypt]
            [happy-app.utilities.common     :as utils]
            [happy-app.utilities.db         :as db]
            [happy-app.utilities.exception  :as ex]))

(defn fetch
  "Summary:   Reads a user from the db.
   Condition: id is a positive number."

  [clause query spec]
  (try
    (jdbc/with-db-connection [db spec]
      (jdbc/query db
                  [query clause]))
    (catch Exception e (ex/handle-exception e))))

(defn create!
  "Summary:   Creates a user's account in the db.
   Condition: user is a map containing an email and a password key."

  [request table spec]
  (try
    (jdbc/with-db-connection [db spec]
      (jdbc/insert! db
                    table
                    request))
    (catch Exception e (ex/handle-exception e))))

(defn update!
  "Summary:       Modifies the users record in the db.
   Condition(s):  id is a positive number.
                  updated is a non-empty map."

  [user-id updated table spec]
    (try
      (jdbc/with-db-connection [db spec]
        (jdbc/update! db
                      table
                      (utils/sanitize-keys updated)
                      ["id = ?" user-id]))
      (catch Exception e (ex/handle-exception e))))
