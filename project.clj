(defproject happy-app "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/java.jdbc "0.3.7"]
                 [org.clojure/tools.logging "0.3.1"]
                 [org.slf4j/slf4j-log4j12 "1.7.12"]
                 [log4j/log4j "1.2.17" :exclusions [javax.mail/mail
                                                    javax.jms/jms
                                                    com.sun.jmdk/jmxtools
                                                    com.sun.jmx/jmxri]] 
                 [clojure.jdbc/clojure.jdbc-c3p0 "0.3.1"]
                 [org.postgresql/postgresql "9.4-1201-jdbc41"]
                 [ring/ring-jetty-adapter "1.4.0"]
                 [ring/ring-json "0.4.0"]
                 [lein-ring "0.9.6"]
                 [compojure "1.4.0"]
                 [clojurewerkz/scrypt "1.2.0"]
                 [clojure.jdbc/clojure.jdbc-c3p0 "0.3.2"]
                 [com.mchange/c3p0 "0.9.5.1"]
                 [crypto-random "1.2.0"]
                 [clj-time "0.11.0"]
                 [slingshot "0.12.2"]
                 [buddy/buddy-sign "0.6.1"]
                 [http-kit "2.1.18"]]
  :main ^:skip-aot happy-app.web
  :ring   {:handler happy-app.web/app}
  :uberjar-name "happy-app-standalone.jar"
  :profiles {:uberjar {:aot :all}})
