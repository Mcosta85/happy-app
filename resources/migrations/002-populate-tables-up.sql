-- Populate the Status table

INSERT INTO status (name, description)
VALUES ('Active', 'Active items are pretty self explanatory.');

INSERT INTO status (name, description)
VALUES ('Inactive', 'Inactive items arent quite deleted, and not quite active. They are very much a paradox');

INSERT INTO status (name, description)
VALUES ('Deleted', 'Deleted items have been scheduled for deletion by their owner or an admin. This is a soft delete. For all intents and purposes, it does not exist.');

INSERT INTO status (name, description)
VALUES ('Pending', 'Pending items are generally newly created items which need some approval before being shifted to active.');

INSERT INTO status (name, description)
VALUES ('Needs Review', 'Items needing review have been flagged as potentially violating TOU. These must be cleared by an admin.');

-- Populate the Role table

INSERT INTO role (name, description)
VALUES ('Admin', 'Admins have access to every bit of data in the system. This role is reserved for Happy App staff only. DO NOT GIVE THIS TO A USER.');

INSERT INTO role (name, description)
VALUES ('Standard', 'This is the default role given to all users.');
