REVOKE ALL on user_flag from dbuser;
DROP TABLE user_flag;

REVOKE ALL on message_flag from dbuser;
DROP TABLE message_flag;

REVOKE ALL on flag from dbuser;
DROP TABLE flag;

REVOKE ALL on user_setting from dbuser;
DROP TABLE user_setting;

REVOKE ALL on user_feeling from dbuser;
DROP TABLE user_feeling;

REVOKE ALL on message_tag from dbuser;
DROP TABLE message_tag;

REVOKE ALL on tag from dbuser;
DROP TABLE tag;

REVOKE ALL on feeling from dbuser;
DROP TABLE feeling;

REVOKE ALL on friendship from dbuser;
DROP TABLE friendship;

REVOKE ALL on user_message from dbuser;
DROP TABLE user_message;

REVOKE ALL on message from dbuser;
DROP TABLE message;

REVOKE ALL on user_role from dbuser;
DROP TABLE user_role;

REVOKE ALL on user_detail from dbuser;
DROP TABLE user_detail;

REVOKE ALL on user_base from dbuser;
DROP TABLE user_base;

REVOKE ALL on role from dbuser;
DROP TABLE role;

REVOKE ALL on status from dbuser;
DROP TABLE status;

REVOKE ALL on message_id_seq from dbuser;
REVOKE ALL on role_id_seq from dbuser;
REVOKE ALL on status_id_seq from dbuser;
REVOKE ALL on feeling_id_seq from dbuser;
REVOKE ALL on tag_id_seq from dbuser;
REVOKE ALL on user_base_id_seq from dbuser;
REVOKE ALL on flag_id_seq from dbuser;

DROP ROLE dbuser;
