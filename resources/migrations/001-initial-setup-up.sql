CREATE TABLE status (
  id          serial        PRIMARY KEY,
  description varchar(250),
  name        varchar(50)   NOT NULL
);

CREATE TABLE role (
  id          serial        PRIMARY KEY,
  name        varchar(25)   NOT NULL,
  description varchar(250)  NOT NULL,
  status_id   integer       DEFAULT 2 references status(id)
);

CREATE TABLE user_base (
  id                serial        PRIMARY KEY,
  email             varchar(50)   UNIQUE NOT NULL,
  status_id         integer       references status(id),
  password          varchar(150)  NOT NULL,
  salt              varchar(150)  NOT NULL,
  is_verified       boolean       DEFAULT false,
  role_id           integer       references role(id),
  name              varchar(50)
);

CREATE TABLE user_role (
  user_id   integer   references user_base(id),
  role_id   integer   references role(id)
);

CREATE TABLE message (
  id          serial PRIMARY KEY,
  title       varchar(50)   NOT NULL,
  body        varchar(250)  NOT NULL,
  status_id   integer       references status(id),
  user_id     integer       references user_base(id),
  created_on  date          NOT NULL
);

CREATE TABLE user_message (
  user_id     integer references user_base(id),
  message_id  integer references message(id),
  is_read     boolean DEFAULT false,
  read_at     timestamp
);

CREATE TABLE friendship (
  initiator   integer references user_base(id),
  user_id     integer references user_base(id)
);

CREATE TABLE feeling (
  id          serial        PRIMARY KEY,
  name        varchar(50)   NOT NULL,
  description varchar(255)  NOT NULL,
  status_id   integer       references status(id)
);

CREATE TABLE tag (
  id          serial        PRIMARY KEY,
  name        varchar(50)   NOT NULL,
  description varchar(250)  NOT NULL,
  status_id   integer       references status(id)
);

CREATE TABLE message_tag (
  message_id  integer references message(id),
  tag_id      integer references tag(id)
);

CREATE TABLE user_feeling (
  user_id     integer   references user_base(id),
  feeling_id  integer   references feeling(id),
  date_felt   date      NOT NULL
);

CREATE TABLE user_setting (
  user_id             integer  references user_base(id),
  random_message      boolean  NOT NULL,
  push_notifications  boolean  NOT NULL,
  display_real_name   boolean  NOT NULL
);

CREATE TABLE flag (
  id          serial        PRIMARY KEY,
  name        varchar(50)   NOT NULL,
  description varchar(250)  NOT NULL,
  status_id   integer       references status(id)
);

CREATE TABLE message_flag (
  message_id  integer  references message(id),
  flag_id     integer  references flag(id)
);

CREATE TABLE user_flag (
  user_id integer references user_base(id),
  flag_id integer references flag(id)
);

-- Create needed dbuser
CREATE ROLE dbuser LOGIN
  ENCRYPTED PASSWORD 'md5e6b54889910738ab4c6efd5cfbbe98f1'
  NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;

-- Grant all needed permissions to dbuser
GRANT SELECT on status to dbuser;
GRANT SELECT, INSERT, UPDATE on user_base to dbuser;
GRANT SELECT on role to dbuser;
GRANT SELECT on user_role to dbuser;
GRANT SELECT on message to dbuser;
GRANT SELECT, INSERT, UPDATE on user_message to dbuser;
GRANT SELECT, UPDATE, INSERT on friendship to dbuser;
GRANT SELECT on feeling to dbuser;
GRANT SELECT on tag to dbuser;
GRANT SELECT, INSERT, UPDATE on message_tag to dbuser;
GRANT SELECT, INSERT, UPDATE on user_feeling to dbuser;
GRANT SELECT, INSERT, UPDATE on user_setting to dbuser;
GRANT SELECT on flag to dbuser;
GRANT SELECT, INSERT on message_flag to dbuser;
GRANT SELECT, INSERT on user_flag to dbuser;

GRANT SELECT, UPDATE on flag_id_seq to dbuser;
GRANT SELECT, UPDATE on user_base_id_seq to dbuser;
GRANT SELECT, UPDATE on tag_id_seq to dbuser;
GRANT SELECT, UPDATE on feeling_id_seq to dbuser;
GRANT SELECT, UPDATE on status_id_seq to dbuser;
GRANT SELECT, UPDATE on role_id_seq to dbuser;
GRANT SELECT, UPDATE on message_id_seq to dbuser;
